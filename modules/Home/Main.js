import styles from './Main.module.scss'

export default function Main() {
    return (
      <div className={styles.main}>
        <h1>IXЛRI<span>α</span></h1>
        <h2>We are building a healthier, more peaceful, more enlightening<br></br> and more stable place to live. </h2>
      </div>
    )
  }
  