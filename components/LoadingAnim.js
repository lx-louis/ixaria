import Lottie from "react-lottie";
import animationData from "./LoadingAnim.json";

function LoadingAnim() {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return <Lottie options={defaultOptions} height={30} width={30} />;
}

export default LoadingAnim;