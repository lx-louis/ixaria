import Button from '../components/Button'
import styles from './Header.module.scss'
import Link from 'next/link'
import LoadingAnim from '../components/LoadingAnim'
import Image from 'next/image'
export default function Header() {
    return (
      <div className={styles.header}>
        <div className={styles.container}>
          <div className={styles.container_left}>
          <svg xmlns="http://www.w3.org/2000/svg" className={styles.home_icon} viewBox="0 0 20 20" fill="currentColor">
            <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
          </svg>
            <Link href="#" className={styles.link}>
              <div>Mirror (comming soon)</div>
            </Link>
          </div>
          <div className={styles.container_right}>
            <Button/>
          </div>
        </div>
        <div className={styles.dev}> 
          <LoadingAnim /> Coming soon
        </div>
      </div>
    )
}